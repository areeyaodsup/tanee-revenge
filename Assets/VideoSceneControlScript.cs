﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class VideoSceneControlScript : MonoBehaviour {

    //private VideoPlayer videoPlayer;
    const int CountToChange = 50;
    float timeCounter;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timeCounter += Time.deltaTime;
        Debug.Log(timeCounter);
        if (timeCounter >= CountToChange)
        {
            SceneManager.UnloadScene("VideoScene");
            SceneManager.LoadScene("MenuScene");
        }

    }
}
