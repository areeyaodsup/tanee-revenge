﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class MainMenuControlScript : MonoBehaviour{

    [SerializeField] Button _startButton;
    [SerializeField] Button _optionsButton;
    [SerializeField] Button _exitButton;

    AudioSource _audioSource;
    [SerializeField] AudioClip _holdOverClip;

    // Use this for initialization
    void Start() {
        _audioSource = this.GetComponent<AudioSource>();
        _audioSource.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];

        //_startButton.onClick.AddListener(() => { OnPointerEnter(); });
        //_optionsButton.onClick.AddListener(() => { OnPointerEnter(); });
        //_exitButton.onClick.AddListener(() => { OnPointerEnter(); });

        SetupButtonsDelegate();

         //if (!SoundManager.Instance.BGMSource.isPlaying)
         //   SoundManager.Instance.BGMSource.Play();
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void OnPointerEnter()
    {
        if (_audioSource.isPlaying)
            _audioSource.Stop();

        _audioSource.PlayOneShot(_holdOverClip);
        Debug.Log("voice play");
    }

    private void SetupButtonsDelegate()
    {
        _startButton.onClick.AddListener(delegate {
            StartButtonClick(_startButton);

        });

        _optionsButton.onClick.AddListener(delegate {
            OptionsButtonClick(_optionsButton);

        });

        _exitButton.onClick.AddListener(delegate {
            ExitButtonClick(_exitButton);

        });
    }

    public void StartButtonClick(Button button)
    {
        SceneManager.LoadScene("Coregame_dev");
    }

    public void OptionsButtonClick(Button button)
    {
        if (!GameApplicationManager.Instance.IsOptionMenuActive)
        {
            SceneManager.LoadScene("OptionScene", LoadSceneMode.Additive);
            GameApplicationManager.Instance.IsOptionMenuActive = true;
        }
    }
    
    public void ExitButtonClick(Button button)
    {
        Application.Quit();
    }
   
}
