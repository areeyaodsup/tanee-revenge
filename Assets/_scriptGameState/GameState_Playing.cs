﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;


public class GameState_Playing : GameState
{
    bool _isStateActive = false;

    public override bool IsStateActive
    {
        get { return _isStateActive; }
        set { _isStateActive = value; }
    }

    [Header("Player")]
    [SerializeField]
    protected GameObject _playerObject;
    [SerializeField]
    protected GameObject _atkBox;
    protected ComboScoreComponent _cScore;

    [Header("Boss")]
    [SerializeField]
    protected GameObject _bossObject;
    
    [Header("System")]
    [SerializeField]
    protected GameObject _waveSystem;
    [SerializeField]
    protected GameObject _waveBoss;

    [Header("UIs")]
    [SerializeField]
    protected Text _textScore;
    [SerializeField]
    protected Text _comboScore;
    [SerializeField]
    protected Text _waveScore;

    [Header("VFX")]
    [SerializeField]
    protected GameObject _deadPlayer;
    
    private float searchCountdown = 1f;

    public override void Enter(GameState from)
    {
        _cScore = _atkBox.GetComponent<ComboScoreComponent>();

        Dash dash = _playerObject.GetComponent<Dash>();
        dash.enabled = true;

        WaveSpawner ws = _waveSystem.GetComponent<WaveSpawner>();
        ws.enabled = true;

    }

    public override void Exit(GameState to)
    {
        //_waveSystem.gameObject.SetActive(false);
        GameObject go = Instantiate(_deadPlayer, _playerObject.transform.position, Quaternion.identity);
        Destroy(go, 0.2f);

        _playerObject.SetActive(false);
        _comboScore.gameObject.SetActive(false);
        _waveBoss.gameObject.SetActive(false);
        _waveScore.gameObject.SetActive(false);
    }

    public override void Tick()
    {
        if (!_isStateActive) return;
        
        _textScore.text = "Score :" + _cScore.Score;
        _comboScore.text = "Combo :" + _cScore.Combo;

        if (!_waveSystem.activeSelf)
        {
            _bossObject.SetActive(true);
            BossControlWave bcw = _waveBoss.GetComponent<BossControlWave>();
            bcw.enabled = true;
        }

        if (_playerObject.GetComponent<LifePointComponent>().Lifepoint == 0)
        {
            Dash dash = _playerObject.GetComponent<Dash>();
            dash.enabled = false;
         
            if (EnemyKills())
            GameStateManager.Instance.SwitchToState("GameOver");
        }
    
    }

    bool EnemyKills()
    {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0)
        {
            searchCountdown = 1f;
            if(GameObject.FindGameObjectsWithTag("Enemies").Length==0||
                GameObject.FindGameObjectsWithTag("Ball").Length==0||
                GameObject.FindGameObjectsWithTag("HandBoss").Length==0)
            {
                return true;
            }
            GameObject[] go = GameObject.FindGameObjectsWithTag("Enemies");
            foreach (GameObject kill in go)
                Destroy(kill);

            GameObject[] ball = GameObject.FindGameObjectsWithTag("Ball");
            foreach (GameObject kill in ball)
                Destroy(kill);

            GameObject[] hand = GameObject.FindGameObjectsWithTag("HandBoss");
            foreach (GameObject kill in hand)
                Destroy(kill);

        }
        return false;
    }

    public override string GetName()
    {
        return "Playing";
    }
}
