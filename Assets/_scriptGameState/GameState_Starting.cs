﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using TMPro;

public class GameState_Starting :GameState {

    bool _isStateActive = false;

    public override bool IsStateActive
    {
        get { return _isStateActive; }
        set { _isStateActive = value; }
    }

    [SerializeField]
    Text _textReady;
    int _secondCounter;
    const int COUNTING_TO_START = 3;

    public ButtonState _playButton;

    public override void Enter(GameState from)
    {
      
    }

    public override void Exit(GameState to)
    {
       
    }

    public override void Tick()
    {
        if (_playButton._currentState == ButtonState.State.Up)
        {
            GameStateManager.Instance.SwitchToState("Playing");
        }
    }

    public override string GetName()
    {
        return "Starting";
    }

 }
