﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager :MonoBehaviour{
    static public GameStateManager Instance{
        get{
            if(_singletonInstance==null){
                _singletonInstance = GameObject.FindObjectOfType<GameStateManager>();
                GameObject container = new GameObject("GameStateManager");
                _singletonInstance = container.AddComponent<GameStateManager>();
            }
            return _singletonInstance;
        }
    }
    static protected GameStateManager _singletonInstance;

    public GameState[] _gameState;
    protected Stack<GameState> _gameStatesStack=new Stack<GameState>();
    protected Dictionary<string, GameState> _gameStateDict = new Dictionary<string, GameState>();

	void Awake()
	{
		//Assign our singleton instance
        if(_singletonInstance==null){
            _singletonInstance = this;
        }else{
            if(this!=_singletonInstance){
                Destroy(this.gameObject);
            }
        }
        //Add gameState into list and Dict fpr esy access/finding
        for (int i = 0; i < _gameState.Length;i++)
        {
            _gameState[i].manager = this;
            _gameStateDict.Add(_gameState[i].GetName(), _gameState[i]);
        }

        _gameStatesStack.Clear();
        PushState("Playing"); //first state start
	}

	void Start()
	{
		
	}

	void Update()
	{
        if(_gameStatesStack.Count>0){
            _gameStatesStack.Peek().Tick();
        }	
	}

    void PushState(string gameStateName)
    {
        GameState nextGameState;
        nextGameState = FindGameStateInDict(gameStateName);

        if(_gameStatesStack.Count>0){
            GameState previousGameState = _gameStatesStack.Pop();
            previousGameState.Exit(nextGameState);
            previousGameState.IsStateActive = false;

            nextGameState.Enter(previousGameState);
            nextGameState.IsStateActive = true;
        }
        else
        {
            nextGameState.Enter(null);
            nextGameState.IsStateActive = true;
        }
        _gameStatesStack.Push(nextGameState);
    }

    public void PopState(String gameStateName){
        if(_gameStatesStack.Count<2)
        {
            Debug.LogError("Can't pop states,only one in stack.");
            return;
        }
    }

    public void SwitchToState(string newState)
    {
        GameState nextGamestate = FindGameStateInDict(newState);
        if(nextGamestate==null)
        {
            Debug.Log("Can't find the state named "+newState);
            return;
        }

        GameState topGameState = _gameStatesStack.Pop();

        topGameState.Exit(nextGamestate);
        topGameState.IsStateActive = false;

        nextGamestate.Enter(topGameState);
        nextGamestate.IsStateActive = true;

        _gameStatesStack.Push(nextGamestate);
    }

    public GameState FindGameStateInDict(string gameStateName)
    {
        GameState gameState;
        if(!_gameStateDict.TryGetValue(gameStateName,out gameState))
        {
            Debug.Log("Can't find the state named "+name);
            return null;
        }
        return gameState;
    }
}