﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using System.IO;
using System;

public class GameState_Winning : GameState
{

    bool _isStateActive = false;

    public override bool IsStateActive
    {
        get { return _isStateActive; }
        set { _isStateActive = value; }
    }


    [SerializeField]
    Text _textWinning;
  
    float _secondCountToEnd;
    const float TIME_TO_CHANGE = 5;

    public override void Enter(GameState from)
    {
        this.gameObject.SetActive(true);
        from.gameObject.SetActive(false);
        _secondCountToEnd = 0;


        Debug.Log("you win");
        _textWinning.gameObject.SetActive(true);

    }

    public override void Exit(GameState to)
    {
        _textWinning.gameObject.SetActive(false);
    }

    public override void Tick()
    {
        if (!_isStateActive) return;
        
        _secondCountToEnd += Time.deltaTime;

        if (_secondCountToEnd >= TIME_TO_CHANGE)
        {
            GameStateManager.Instance.SwitchToState("ScoreBoard");
        }

    }

    public override string GetName()
    {
        return "Winning";

    }
}
