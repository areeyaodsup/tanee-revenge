﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

using UnityEngine.UI;

public class GameState_GameOver : GameState {

    bool _isStateActive = false;

    public override bool IsStateActive
    {
        get{return _isStateActive;}
        set { _isStateActive = value; }
    }

    [SerializeField]
    Text _textGameOver;
    [SerializeField]
    Text _textGameOverWave;

    float _secondCountToEnd;
    const float TIME_TO_CHANGE = 5;

    public override void Enter(GameState from)
    {
        this.gameObject.SetActive(true);
        from.gameObject.SetActive(false);
        _secondCountToEnd = 0;

        Debug.Log("you lose");
        _textGameOver.gameObject.SetActive(true);
        _textGameOverWave.gameObject.SetActive(true);
    }

    public override void Exit(GameState to)
    {
       _textGameOver.gameObject.SetActive(false);
        _textGameOverWave.gameObject.SetActive(false);
    }

   public override void Tick()
    {
        if (!_isStateActive) return;
        
        _secondCountToEnd += Time.deltaTime;
        
        if(_secondCountToEnd>=TIME_TO_CHANGE){
            GameStateManager.Instance.SwitchToState("ScoreBoard");
        }
    }

    public override string GetName()
    {
        return "GameOver";
    }
}
