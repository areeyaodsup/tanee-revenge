﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameState : MonoBehaviour {
    [HideInInspector]
    public GameStateManager manager;
    public abstract bool IsStateActive { get; set; }

    public abstract void Enter(GameState from);
    public abstract void Exit(GameState to);
    public abstract void Tick();

    public abstract string GetName(); 
}
