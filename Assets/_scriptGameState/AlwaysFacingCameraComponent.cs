﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysFacingCameraComponent : MonoBehaviour {
    public Camera m_camera;
	private void LateUpdate()
	{
        transform.LookAt(transform.position+m_camera.transform.rotation *
                         Vector3.forward,
                         m_camera.transform.rotation * Vector3.up);
	}

}
