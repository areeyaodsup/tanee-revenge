﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class GameState_Scoreboard : GameState
{
    bool _isStateActive = false;

    public override bool IsStateActive
    {
        get { return _isStateActive; }
        set { _isStateActive = value; }
    }

    [SerializeField]
    Text _hightScore;

    [SerializeField]
    Text _myScore;

    [SerializeField]
    protected GameObject _atkBox;
    protected ComboScoreComponent _cScore;

   // [SerializeField]
   // Button _backButton;
   // Button _exitButton;

    public ButtonState _backButton;
    public ButtonState _exitButton;

    protected int _mScore;
    [SerializeField]
    protected int _hScore;
    private string _saveFolderLocation;

    public override void Enter(GameState from)
    {
        _cScore = _atkBox.GetComponent<ComboScoreComponent>();
        this.gameObject.SetActive(true);
       
        if (!Directory.Exists("Assets/Resources/Save"))
            Directory.CreateDirectory("Assets/Resources/Save");
        _saveFolderLocation = "Assets/Resources/Save";

        LoadGame();

        _mScore = _cScore.Score;
        _hScore = GameApplicationManager.Instance.HightScore;
    }

    public override void Exit(GameState to)
    {
        this.gameObject.SetActive(false);
    }

    public override void Tick()
    {
        _myScore.text = _mScore.ToString();
        _hightScore.text = _hScore.ToString();

        if (_backButton._currentState == ButtonState.State.Up)
        {
            SceneManager.LoadScene("MenuScene");
        }
        if (_exitButton._currentState == ButtonState.State.Up)
        {
            Application.Quit();
        }

        if (_mScore > 0)
        {
            if (_hScore > _mScore)
            {
                GameApplicationManager.Instance.HightScore = _hScore;
            }
            else if (_hScore < _mScore)
            {
                GameApplicationManager.Instance.HightScore = _mScore;
            }
        }
        SaveGame();
    }

    void SaveGame()
    {
        GameSaveHighScore gsh = new GameSaveHighScore
        {
            _highscore = GameApplicationManager.Instance.HightScore
        };

        string filename = "HighScore";
        string location = Path.Combine(_saveFolderLocation, filename);

        
        ISaveLoadGameSaveHighScore islg = new SaveLoadGameSaveHighScoreJSON();
        location = location + ".json";

        islg.SaveGameSaveHighScore(gsh, location);

    }

    void LoadGame()
    {
        string filename = "HighScore";
        string location = Path.Combine(_saveFolderLocation, filename);
        TextAsset targetFile = Resources.Load<TextAsset>("Saves/" + filename);

            ISaveLoadGameSaveHighScore islg = new SaveLoadGameSaveHighScoreJSON();
            location = location + ".json";

            GameSaveHighScore gsh = new GameSaveHighScore();
            gsh = islg.LoadGameSaveHighScore(location);

            GameApplicationManager.Instance.HightScore = gsh._highscore;
            // Debug.Log("Load gsh" + gsh._highscore);
            //Debug.Log("Load " + GameApplicationManager.Instance.HightScore);  
        
          
    }


    public override string GetName()
    {
        return "ScoreBoard";
    }

}
