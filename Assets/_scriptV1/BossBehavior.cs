﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehavior : MonoBehaviour {

    GameObject _patk;
    public GameObject _exploEffect;
    
	// Use this for initialization
	void Start () {
        _patk = GameObject.FindGameObjectWithTag("PATK");
	}
	
	// Update is called once per frame
	void Update () {
		if(this.GetComponent<LifePointComponent>().Lifepoint==0)
            GameStateManager.Instance.SwitchToState("Winning");
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag.Contains("Ball"))
        {
            LifePointComponent lpc = this.GetComponent<LifePointComponent>();
            lpc.SubtractLifePoint(1);

            _patk.GetComponent<ComboScoreComponent>().AddScore(75);

            GameObject effect = Instantiate(_exploEffect, other.transform.position, Quaternion.identity);
            Destroy(effect, 0.5f); 
            Debug.Log("Hit by ball");
            Destroy(other.gameObject,0.1f);

        }
    }
}
