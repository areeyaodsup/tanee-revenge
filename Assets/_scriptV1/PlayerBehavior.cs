﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    IDLE,MOVE,ATTACK
};

public enum Direction
{
    NONE,LEFT,RIGHT
};

[RequireComponent(typeof(Dash))]
[RequireComponent(typeof(Sense))]
[RequireComponent(typeof(PrototypeTracking))]

public class PlayerBehavior : MonoBehaviour {

    public PlayerState m_state = PlayerState.IDLE;
    public GameObject _colliderBox;
    [Header("VFX")]
    public GameObject _attackVFX;
    public GameObject _missText;

    Dash _dashCheck;
    Sense _checkEnemy;
    PrototypeTracking _trackingCheck;
          
    //player move | attack
     //private int _health = 3;
     public int _effectSpeed = 1;
     private Vector3 colliderDis = new Vector3(1,0,0);

    AudioSource _audioSource;
    [SerializeField]
    AudioClip _punchSound;

    [Header("Stunt")]
    [SerializeField]
    float countDown;
    public float DelayState = 3f;
    // Use this for initialization
    void Start()
    {
       _dashCheck = this.GetComponent<Dash>();
       _checkEnemy = GetComponent<Sense>();
        countDown = DelayState;
        _audioSource = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void FixedUpdate()
    {
       
        switch (m_state){
            case PlayerState.MOVE:
                MoveState();
                break;
            case PlayerState.ATTACK:
                AttackState();
                break;
            case PlayerState.IDLE:
                IdleState();
                break;
        }
     
        //Debug.Log("State "+m_state);
        // Debug.Log("Direction " + m_direction);
    }

    private void IdleState()
    {
        Collider m_box = _colliderBox.GetComponent<Collider>();
        //_dashCheck.enabled = true;

        if (_dashCheck.m_direct == Direction.LEFT || _dashCheck.m_direct == Direction.RIGHT)
        {
            if (_checkEnemy.bestTarget == null)
            {
                m_state = PlayerState.MOVE;
            }
            else
            {
                m_state = PlayerState.ATTACK;
                m_box.enabled = !m_box.enabled;
            }
         
        }
       
    }
    bool active = false;
    bool activeMove = false;

    private void MoveState()
    {
        ComboScoreComponent csc = _colliderBox.GetComponent<ComboScoreComponent>();

       if (!activeMove)
            {
                csc.DecreaseCombo(2.0f);
                 activeMove = true;
                // _dashCheck.enabled = false;

            GameObject go = Instantiate(_missText,new Vector3(this.transform.position.x,4f,0),this.transform.rotation);
            Destroy(go, 0.3f);
        }

      
        if (_dashCheck.m_direct == Direction.NONE)
        {
            _dashCheck.enabled = false;

            if (countDown <= 0)
            {
                countDown = DelayState;
                _dashCheck.enabled = true;
                activeMove = false;
                m_state = PlayerState.IDLE;

                Debug.Log("Move enable");
            }
            else
            {
                countDown -= Time.deltaTime;
            }
        }
       
    }

    private void AttackState()
    {
        Collider m_box = _colliderBox.GetComponent<Collider>();
        Vector3 playerPosi = this.transform.position;
        Vector3 bestAtkDis = colliderDis;
        if (_audioSource.isPlaying)
            _audioSource.Stop();

        _audioSource.PlayOneShot(_punchSound);

        if (_checkEnemy.bestTarget != null)
        {
            bestAtkDis = _checkEnemy.bestTarget.transform.position;
           // print("BestTar " + bestAtkDis);
            bestAtkDis.Normalize();

            if (bestAtkDis.x < 0)
                bestAtkDis.x *= -1;
            //print("BestTar " + bestAtkDis);
        }
               
        if (_dashCheck.m_direct == Direction.LEFT)
        {
            _colliderBox.transform.position = new Vector3(-bestAtkDis.x+ playerPosi.x, 0.5f, bestAtkDis.z + playerPosi.z);
            //print("LEFT " + _colliderBox.transform.position);
            PunchEffect(_attackVFX, true);
        }
        else if (_dashCheck.m_direct == Direction.RIGHT)
        {
            _colliderBox.transform.position = new Vector3(bestAtkDis.x + playerPosi.x, 0.5f , bestAtkDis.z + playerPosi.z);
            //print("Right" + _colliderBox.transform.position);
            PunchEffect(_attackVFX, false);
        }

        if (_dashCheck.m_direct == Direction.NONE)
        {
            m_state = PlayerState.IDLE;
            _colliderBox.transform.position =this.transform.position;
            m_box.enabled = !m_box.enabled;
            active = false;
        }


    }

    void PunchEffect(GameObject effect,bool flip)
    {
      if (!active)
        {
            active = true;
            GameObject punch = Instantiate(effect, this.transform.position, Quaternion.identity);
            punch.GetComponent<SpriteRenderer>().flipX = flip;
            if (flip == false)
                punch.transform.Translate(Vector3.right * _effectSpeed);
            else
                punch.transform.Translate(Vector3.left * _effectSpeed);

            Destroy(punch, 0.5f);


        }

    }


    void OnTriggerEnter(Collider colli)
    {
        if (colli.tag.Contains("EATK"))
        {
            ComboScoreComponent csc = _colliderBox.GetComponent<ComboScoreComponent>();
            LifePointComponent lpc = this.GetComponent<LifePointComponent>();
            csc.DivisCombo(2.0f);
            lpc.SubtractLifePoint(1);
            print("Hit by Enemies");

            colli.enabled = false;
           
        }
        if (colli.tag.Contains("Ball")&&colli.isTrigger==true)
        {
            ComboScoreComponent csc = _colliderBox.GetComponent<ComboScoreComponent>();
            LifePointComponent lpc = this.GetComponent<LifePointComponent>();
            csc.DivisCombo(2.0f);
            lpc.SubtractLifePoint(1);
            print("Hit by Enemies Ball");

           Destroy(colli.gameObject);
          
        }
}

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag.Contains("HandBoss"))
        {
            LifePointComponent lpc = this.GetComponent<LifePointComponent>();
            lpc.SubtractLifePoint(2);
            print("Hit by Enemies Boss");

            Destroy(other.gameObject, 0.1f);
        }

    }
}

   

