﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationController : MonoBehaviour {

    EnemyBehavior _enemy;
    Animator _animator;
    SpriteRenderer _sprite;

    // Use this for initialization
	void Start () {
        _enemy = GetComponent<EnemyBehavior>();
        _animator = GetComponent<Animator>();
        _sprite = GetComponent<SpriteRenderer>();
	}
/*
    // Update is called once per frame
    void FixedUpdate()
    {

        //_sprite.flipX
        switch (EnemyBehavior.Insctance.m_eDirect)
        {
            
            case EDirect.LEFT:
                _sprite.flipX = true; // left of player
                break;
            case EDirect.RIGHT:
                _sprite.flipX = false; //right of player
                break;
        }
    }
    */
    public void FlipMe(bool flip)
    {
        _sprite.flipX = flip;
    }
}
