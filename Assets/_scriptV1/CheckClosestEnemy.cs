﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckClosestEnemy : MonoBehaviour {
   
    public Transform GetClosestEnemy (Transform[] target)
    {
        //Transform[] enemies = 
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = this.transform.position;

        foreach(Transform potentialTarget in target)
        {
            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget.transform;
            }
        }
        return bestTarget;
    }

}
