﻿
using System;
using UnityEngine;

public class Sense : MonoBehaviour {

    public float checkRadius;
    public LayerMask checkLayers;
    public Transform bestTarget = null;

    void Update()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, checkRadius, checkLayers);
        Array.Sort(colliders, new DistanceComparer(transform));

        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = this.transform.position;

        foreach(Collider target in colliders)
        {
            Vector3 directionToTarget = target.transform.position - currentPosition;
            float dSqrTotarget = directionToTarget.sqrMagnitude;
            if (dSqrTotarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrTotarget;
                bestTarget = target.transform;
            }
          //  print(bestTarget.transform.position);
        }
        if (bestTarget != null)
            Debug.DrawLine(this.transform.position, bestTarget.transform.position);

    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, checkRadius);
    }
}
