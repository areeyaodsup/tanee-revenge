﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour {

    public enum SpawnState
    {
        SPAWNING,WAITING,COUNTING
    };

    [System.Serializable]
    public class Wave
    {
        public string name;
        public Transform[] enemy;
        public int count;
        public float rate; //rate to spawn
    }

    [Header("Text Wave")]
    [SerializeField]
    Text _wavetext;
    [SerializeField]
    Text _gameOverwave;

    [Header("Wave Controller")]
    public Wave[] waves;
    private int nextWave = 0;

    public Transform[] spawnPoint;

    public float timeBetweenWaves = 5f;
    public float waveCountdown;

    private float searchCountdown = 1f;

    private SpawnState state = SpawnState.COUNTING;

    void Start()
    {
        if (spawnPoint.Length == 0)
        {
            Debug.LogError("No spawn point referenced.");
        }

        waveCountdown = timeBetweenWaves;
    }

    void Update()
    {
        if (state == SpawnState.WAITING)
        {
            //check enemy still alive
            if (!EnemyIsAlive())
            {
                //begin new round
                // Debug.Log("Wave Completed!");
                  WaveCompleted();
                return;
            }
            else
            {
                return;
            }
        }

        if (waveCountdown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                //start spawning wave
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountdown -= Time.deltaTime;
        }
    }

    private void WaveCompleted()
    {
        Debug.Log("Wave Completed!");

        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;

        if (nextWave + 1>waves.Length-1)
        {
            nextWave = 0;
            Debug.Log("Completed all waves.");
            _wavetext.text = "Final Wave";
            _gameOverwave.text= "Final Wave";
            this.gameObject.SetActive(false);
            // GameStateManager.Instance.SwitchToState("Winning");
        }
        else
        {
            nextWave++;
        }
    }

    bool EnemyIsAlive()
    {
        //Debug.Log(GameObject.FindGameObjectsWithTag("Enemies").Length);
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectsWithTag("Enemies").Length == 0) //==null
            {
                Debug.Log("Enemies all defeat");
                return false;
            }
        }
        return true;
       
    }

    IEnumerator SpawnWave(Wave _wave)
    {
        _wavetext.text = "Wave " + _wave.name;
        _gameOverwave.text = "Wave " + _wave.name;
        //Debug.Log("Spawning Wave: "+_wave.name);
        state = SpawnState.SPAWNING;
               
        //spawn
        for (int i = 0; i < _wave.count; i++)
        {
            int rse = Random.Range(0, _wave.enemy.Length);
            SpawnEnemy(_wave.enemy[rse]);
            yield return new WaitForSeconds(1f / _wave.rate);
        }

        state = SpawnState.WAITING;

        yield break;
    }

    void SpawnEnemy(Transform _enemy)
    {
        //spawn
        //Debug.Log("Spawning Enemy: " + _enemy.name);
        Transform _sp = spawnPoint[Random.Range(0,spawnPoint.Length)];//no point set in game
        //Instantiate(_enemy, transform.position, transform.rotation);
        Instantiate(_enemy, _sp.position, _sp.rotation);
    }
}
