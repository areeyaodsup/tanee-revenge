﻿using UnityEngine;
using UnityEngine.UI;

public class LifePointComponent :MonoBehaviour
{
    public int MAX_LEFT_POINT = 3;

    protected int _liftPoint;
    public int Lifepoint{
        get{
            return _liftPoint;
        }
    }

    [SerializeField]
    private Slider _slider = null;

    void Start(){
        _liftPoint = MAX_LEFT_POINT;

        _slider.maxValue = MAX_LEFT_POINT;
       // Debug.Log("LifeHP START"+_liftPoint);
    }

	public void AddLifePoint(int amont){
        _liftPoint += amont;
        if(_liftPoint>MAX_LEFT_POINT){
            _liftPoint = MAX_LEFT_POINT;
        }

       print(_liftPoint);
    }

    public void SubtractLifePoint(int amont){
        _liftPoint -= amont;
        if (_liftPoint < 0)
        {
            _liftPoint = 0;
        }

        _slider.value = _liftPoint;
    }
}