﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyState
{
    FOLLOW,STADY,ATTACK
};

public enum EDirect
{
    LEFT,RIGHT
}
[RequireComponent(typeof(Collision))]
[RequireComponent(typeof(SphereCheck))]
[RequireComponent(typeof(PrototypeTracking))]

public class EnemyBehavior : MonoBehaviour
{
    public EnemyState m_state = EnemyState.FOLLOW;
    public EDirect m_eDirect;
    public GameObject _colliderBox;
    public GameObject _attackVFX;
    public GameObject _myTarget;
    SphereCheck _checkActive;
    PrototypeTracking _trackActive;
    EnemyAnimationController _animation;

    float countDown;
    public float DalayForATKb = 3f;
    public int _effectSpeed = 1;
    public static EnemyBehavior Insctance;

     // Use this for initialization
    void Start()
    {
        Insctance = this;
        _myTarget = GameObject.FindGameObjectWithTag("Player");
        _checkActive = GetComponent<SphereCheck>();
        _trackActive = GetComponent<PrototypeTracking>();
        _animation = GetComponentInChildren<EnemyAnimationController>();
        countDown = DalayForATKb;
    }

    // Update is called once per frame
    void Update()
    {

        //_colliderBox.transform.position = Vector3.zero;
        switch (m_state)
        {
            case EnemyState.FOLLOW:
                FollowState();
                break;
            case EnemyState.STADY:
                StadyState();
                break;
            case EnemyState.ATTACK:

                countDown -= Time.deltaTime;
                if (countDown <= 0)
                {
                    AttackState();
                    countDown = DalayForATKb;
                }
                break;
        }

        AnimationFlip();
        //Debug.Log("State " + m_state);

    }

    private void StadyState()
    {

    }

    private void FollowState()
    {
        // Collider m_box = _colliderBox.GetComponent<Collider>();
        _trackActive.UpdateTracking(_myTarget, this.gameObject);
        _checkActive.UpdateSphere(_myTarget);
        if (_checkActive.activeArea == true)
        {
            m_state = EnemyState.ATTACK;
            // m_box.enabled = !m_box.enabled;
        }

    }

    private void AttackState()
    {
        Collider m_box = _colliderBox.GetComponent<Collider>();
        if (m_box == null)
        {
            Debug.LogError("EATK missing prefab");
            m_state = EnemyState.FOLLOW;
        }

        _checkActive.UpdateSphere(_myTarget);
        _colliderBox.transform.position = PositionCal();

        // print("ATK " + _colliderBox.transform.position);
        m_box.enabled = !m_box.enabled;

        if (_checkActive.activeArea == false)
        {
            _colliderBox.transform.position = new Vector3(this.transform.position.x, 0.5f, this.transform.position.z);
            m_box.enabled = !m_box.enabled;
            m_state = EnemyState.FOLLOW;
            active = false;
        }
    }

    bool active = false;

    private Vector3 PositionCal()
    {
        Vector3 currentPosi = this.transform.position;
        Vector3 targetPosi = _myTarget.transform.position;
        Vector3 Distance = targetPosi - currentPosi;
        Distance.Normalize();
        if (Distance.x < 0)
            Distance.x *= -1;

        if (targetPosi.x > currentPosi.x) //player on right side
        {
            Distance = new Vector3(Distance.x + currentPosi.x, 0.5f, Distance.z + currentPosi.z);
            AttackEffect(_attackVFX, true);
            // print("R " + Distance);
        }
        else if (targetPosi.x < currentPosi.x) //player on left side
        {
            Distance = new Vector3(-Distance.x + currentPosi.x, 0.5f, Distance.z + currentPosi.z);
            AttackEffect(_attackVFX, false);
            // print("L " + Distance);
        }

        return Distance;
    }

    void AnimationFlip()
    {
        Vector3 playerDirection = _myTarget.transform.position;
        //Debug.Log(playerDirection.ToString());
        Vector3 currentDirection = playerDirection - this.transform.position;
       // float _distance = Vector3.Distance(_myTarget.transform.position, this.transform.position);
       // Debug.Log(currentDirection);
        
        //Debug.Log(_distance);
        //if (playerDirection.x < 0)
        //    playerDirection *= -1;
        if (currentDirection.x >= 0) //right of player
        {
            m_eDirect = EDirect.LEFT;
            _animation.FlipMe(true);
            //Debug.Log("Left" + currentDirection.x);
        }
        else// if (currentDirection.x < playerDirection.x) //left of player
        {
            m_eDirect = EDirect.RIGHT;
            _animation.FlipMe(false);
        }
    }

    void AttackEffect(GameObject effect, bool flip)
    {
        if (!active)
        {
            active = true;
            GameObject punch = Instantiate(effect, this.transform.position, Quaternion.identity);
            punch.GetComponent<SpriteRenderer>().flipX = flip;
            if (flip == false)
                punch.transform.Translate(Vector3.left * _effectSpeed+new Vector3(0,1.1f,0));
            else
                punch.transform.Translate(Vector3.right * _effectSpeed+ new Vector3(0, 1.1f, 0));

            Destroy(punch, 0.5f);


        }

    }

}