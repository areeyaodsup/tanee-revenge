﻿using UnityEngine;

public class ScoreComponent : MonoBehaviour {
    protected int _score;
    public int Score
    {
        get { return _score; }
        set { _score = value; }
    }

    public void AddScore(int amount)
    {
        _score += amount;
    }
}
