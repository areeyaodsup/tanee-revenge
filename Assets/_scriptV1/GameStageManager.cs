﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStageManager : MonoBehaviour {

    [SerializeField]
    protected GameObject _comboObject;
    protected ComboScoreComponent _csc;

    [SerializeField]
    protected GameObject _player;
    protected LifePointComponent _lpc;

    [SerializeField]
    Text _scoretext;
    [SerializeField]
    Text _combotext;
    [SerializeField]
    Text _gameovertext;

   

	void Start () {
        _csc = _comboObject.GetComponent<ComboScoreComponent>();
        _lpc = _player.GetComponent<LifePointComponent>();
      }
	
	void Update () {
       
        _scoretext.text = "Score :"+_csc.Score;
        _combotext.text = "Combo " + _csc.Combo;

        if (_lpc.Lifepoint <= 0)
        {
            _gameovertext.enabled=true;
            _gameovertext.text = "GAME OVER";
        }
	}
      
}
