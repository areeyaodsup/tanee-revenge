﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossControlWave : MonoBehaviour {

    public enum SpawnBoss
    {
        SPAWNING,WAITING,COUNTING
    };

    public enum WaveBoss
    {
        BALL,HAND
    };

    public  Transform _ballEnemies;
    public  GameObject _handEnemiesL;
    public GameObject _handEnemiesR;
    public float rate; //เรทปล่อยบอท
    public int count; //จำนวนำ enemy

    private int _nextWave = 0;

    public Transform[] _spawnPoint;
    public float _timeBetween = 2;
    public float _waveCountdown;

    private float searchCountdown = 1f;
   [SerializeField]
    private SpawnBoss state = SpawnBoss.COUNTING;
    private WaveBoss waveState = WaveBoss.BALL;
	// Use this for initialization
	void Start () {
        if (_spawnPoint.Length == 0)
            Debug.LogError("No spawn point referenced.");

        _waveCountdown = _timeBetween;
    }
	
	// Update is called once per frame
	void Update () {
        if (state == SpawnBoss.WAITING)
        {
            if (!EnemyIsAlive())
            {
                WaveCompleted();
                return;
            }
            else
            {
                return;
            }
        }

        if (_waveCountdown <= 0)
        {
            if(state != SpawnBoss.SPAWNING)
            {
                // do something
                if (waveState == WaveBoss.BALL)
                {
                    //Debug.Log("Ball");
                    StartCoroutine(SpawnWave());
                    // Debug.Log("Ball Out");
                }
                else
                {
                   // Debug.Log("Hand");
                    SpawnHand();
                }
              
            }
        }
        else
        {
            _waveCountdown -= Time.deltaTime;
        }
	}

    private void SpawnHand()
    {
        Debug.Log("Hand");
        state = SpawnBoss.SPAWNING;
        Instantiate(_handEnemiesL, new Vector3(-25, 2, 0), Quaternion.identity);
        Instantiate(_handEnemiesR, new Vector3(25, 2, 0), Quaternion.identity);

        state = SpawnBoss.WAITING;
    
    }

    private void SpawnBall()
    {
       
        Transform _sp = _spawnPoint[Random.Range(0, _spawnPoint.Length)];
        Instantiate(_ballEnemies, _sp.position, _sp.rotation);
    }

    bool EnemyIsAlive()
    {
        //Debug.Log(GameObject.FindGameObjectsWithTag("Enemies").Length);
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectsWithTag("Ball").Length == 0) //==null
            {
                Debug.Log("Ball Enemies all defeat");
                return false;
            }

            if (GameObject.FindGameObjectsWithTag("HandBoss").Length == 0)
            {
                Debug.Log("Enemies all defeat");
                return false;
            }
        
        }
        return true;

    }

    private void WaveCompleted()
    {
        Debug.Log("Wave Completed!");
        state = SpawnBoss.COUNTING;
        _waveCountdown = _timeBetween;

        switch (waveState)
        {
            case WaveBoss.BALL:
                waveState = WaveBoss.HAND;
                break;
            case WaveBoss.HAND:
                waveState = WaveBoss.BALL;
                break;
        }

    }

    IEnumerator SpawnWave()
    {
        state = SpawnBoss.SPAWNING;

        for(int i=0;i < count; i++)
        {
            SpawnBall();
            yield return new WaitForSeconds(1f / rate);
        }

        state = SpawnBoss.WAITING;
        rate += 0.25f;
      
        yield break;
    }
}
