﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    public bool _bounds;
    public Vector3 _minCameraPos;
    public Vector3 _maxCameraPos;

	void FixedUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position,desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;

        if(_bounds)
        {
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, _minCameraPos.x,
                _maxCameraPos.x), Mathf.Clamp(transform.position.y, _minCameraPos.y, _maxCameraPos.y),
                Mathf.Clamp(transform.position.z, _minCameraPos.z, _maxCameraPos.z));
        }

      //  transform.LookAt(target);
    }
}
