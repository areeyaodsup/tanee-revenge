﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingPlayer : MonoBehaviour {
    //[SerializeField]
    GameObject _targetObject;
    SphereCheck _checkActive;
    
    public Vector3 _objectMovement = new Vector3(0.1f,0,0);
	// Use this for initialization
	void Start () {
        _targetObject = GameObject.FindGameObjectWithTag("Player");
        _checkActive = GetComponent<SphereCheck>();
    }
	
	// Update is called once per frame
	void Update ()
    {
              
            Vector3 TargetPosiotion = _targetObject.transform.position;
            Vector3 vecToTargetNorm = TargetPosiotion - this.transform.position;
            vecToTargetNorm.Normalize();

            this._objectMovement = vecToTargetNorm / 10.0f;
        if(_checkActive.activeArea==true){
            this.transform.position += Vector3.zero;
        }else{
            this.transform.position += _objectMovement;
            
        }

       
    }

}
