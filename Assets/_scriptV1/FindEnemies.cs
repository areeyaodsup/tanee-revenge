﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindEnemies : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        FindClosest();
	}

    void FindClosest()
    {
        float distanceToClosestEnemy = Mathf.Infinity;
        EnemyBehavior closestEnemy = null;
        EnemyBehavior[] allEnemies = GameObject.FindObjectsOfType<EnemyBehavior>();

        foreach(EnemyBehavior currentEnemy in allEnemies)
        {
            float distanceToEnemy = (currentEnemy.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToEnemy < distanceToClosestEnemy)
            {
                distanceToClosestEnemy = distanceToEnemy;
                closestEnemy = currentEnemy;
            }
        }
        if(closestEnemy != null)
        Debug.DrawLine(this.transform.position, closestEnemy.transform.position);
    }
}
