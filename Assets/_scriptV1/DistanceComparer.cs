﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceComparer : IComparer {

    private Transform compareTransfrom;
    
    public DistanceComparer(Transform compTransfrom)
    {
        compareTransfrom = compTransfrom;
    }

    public int Compare(object x, object y)
    {
        Collider xCollider = x as Collider;
        Collider yCollider = y as Collider;

        Vector3 offset = xCollider.transform.position - compareTransfrom.position;
        float xDistance = offset.sqrMagnitude;

        offset = yCollider.transform.position - compareTransfrom.position;
        float yDistance = offset.sqrMagnitude;

        return xDistance.CompareTo(yDistance);
    }

}
