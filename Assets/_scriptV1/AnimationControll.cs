﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationControll : MonoBehaviour {
  
    Dash _dash;
    Animator _animator;
    SpriteRenderer _sprite;

    const int CHANGE_ANIMATION= 0;
    int _typeAni;
    // Use this for initialization
	void Start () {
        _dash = GetComponent<Dash>();
        _animator = GetComponent<Animator>();
        _sprite = GetComponent<SpriteRenderer>();

        _typeAni = CHANGE_ANIMATION;
	}
	
	// Update is called once per frame
	void Update () {
        switch (Dash.Insctance.m_direct)
        {
            case Direction.LEFT:

                //_animator.SetBool("Punch", true);
                //AnimationChange();
                _animator.SetBool("Punch", true);
                _sprite.flipX = true;
            
                break;
            case Direction.RIGHT:
               
                    _animator.SetBool("Punch", true);
                //_animator.SetBool("UperCut", true);
                    _sprite.flipX = false;
              
                //Debug.Log("punch!!");
                break;
            case Direction.NONE:
                _animator.SetBool("Punch", false);
                //_animator.SetBool("UperCut", false);
                //Debug.Log("not punch!!");
                break;

        }
	}

    void AnimationChange(){
        switch (_typeAni){
            case 1:_animator.SetBool("Punch", true);
                _typeAni += 1;
                break;
            case 2:_animator.SetBool("UperCut", false);
                break;
        }

        Debug.Log(_typeAni);

       if(_typeAni==2){
            _typeAni = CHANGE_ANIMATION;
        }
    }
}
