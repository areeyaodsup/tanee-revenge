﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour {

    public Direction m_direct = Direction.NONE;

    private Rigidbody rb;
    public float dashSpeed;
    private float dashTime;
    public float startDashTime;
    public static Dash Insctance;

    Sense _myDirection;
   
    // Use this for initialization
	void Start () {
        Insctance = this;
        rb = GetComponent<Rigidbody>();
        _myDirection = GetComponent<Sense>();
        dashTime = startDashTime;

    }
	
	// Update is called once per frame
	void FixedUpdate () {

        Vector3 direction=Vector3.zero;
        if (_myDirection.bestTarget != null)
        {
             direction = _myDirection.bestTarget.transform.position;
            direction.Normalize();
        }
        //Vector3 direction = _myDirection.bestTarget.transform.position;
       // direction.Normalize();

        if (m_direct == Direction.NONE)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
                m_direct = Direction.LEFT;
            else if (Input.GetKeyDown(KeyCode.RightArrow))
                m_direct = Direction.RIGHT;
           
            if (Input.touchCount > 0)
            {
               
                Touch myTouch = Input.GetTouch(0);
                if (myTouch.phase == TouchPhase.Began)
                {
                    Vector3 touchPosition = myTouch.position;
                    touchPosition.z = 0;

                    Debug.Log("touch" + touchPosition);
                    if (touchPosition.x > Screen.width/2)
                    {
                        m_direct = Direction.RIGHT;
                    }
                    if (touchPosition.x < Screen.width / 2)
                    {
                        m_direct = Direction.LEFT;
                    }
                }
            }
        }
        else
        {
            if (dashTime <= 0)
            {
                m_direct = Direction.NONE;
                dashTime = startDashTime;
                rb.velocity = Vector3.zero;
            }
            else
            {
                dashTime -= Time.deltaTime;

                if (direction.x != 0)
                {
                   // Debug.Log("Sp Move");
                    if (m_direct == Direction.LEFT)
                    {
                        if (direction.x < 0)
                            direction.x *= -1;
                        rb.velocity = new Vector3 (-direction.x * dashSpeed, direction.y,direction.z);
                       /// print("LEFT " + rb.velocity + " direct" + direction.x);
                    }
                    else if (m_direct == Direction.RIGHT)
                    {
                        if (direction.x < 0)
                            direction.x *= -1;
                        rb.velocity = new Vector3(direction.x * dashSpeed, direction.y, direction.z);
                       /// print("RIGHT " + rb.velocity + " direct" + direction.x);
                    }
                }
                else if(direction.x==0)
                {
                   // Debug.Log("Normal Move");
                    if (m_direct == Direction.LEFT)
                    {
                        rb.velocity = Vector3.left * dashSpeed;
                       /// print(rb.velocity);
                    }
                    else if (m_direct == Direction.RIGHT)
                    {
                        rb.velocity = Vector3.right * dashSpeed;
                       /// print(rb.velocity);
                    }
                   
                }

             }
        }
	}
}
