﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collision))]
[RequireComponent(typeof(SphereCheck))]
[RequireComponent(typeof(PrototypeTracking))]
public class EnemyballBehavior : MonoBehaviour {

    public EDirect m_ebDirect;
    [SerializeField]
    GameObject _myTarget;
    [SerializeField]
    bool isATKPlayer = true;
   // public GameObject _bossTarget;
    PrototypeTracking _trackActive;
    SphereCheck _checkActive;
        
	// Use this for initialization
	void Start () {
        _myTarget = GameObject.FindGameObjectWithTag("Player");
        //_bossTarget = GameObject.FindGameObjectWithTag("Boss");
        _trackActive = GetComponent<PrototypeTracking>();
        _checkActive = GetComponent<SphereCheck>();

	}
	
	// Update is called once per frame
	void Update () {
        _trackActive.UpdateTracking(_myTarget, this.gameObject);
        _checkActive.UpdateSphere(_myTarget);
        /*
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (_myTarget.tag == "Player")
            {
                _myTarget = GameObject.FindGameObjectWithTag("Boss");
            }
            else
            {
                _myTarget = GameObject.FindGameObjectWithTag("Player");
            }
        }*/
	}
   
    void OnTriggerEnter(Collider other)
    {
       // Rigidbody rb = this.GetComponent<Rigidbody>();
        if (other.gameObject.tag == "PATK")
        {
            Collider ballColl = this.GetComponent<Collider>();
            Debug.Log("Hit PATK");
           // rb.velocity = Vector3.zero;
            ballColl.isTrigger = !ballColl.isTrigger; //false
            isATKPlayer = false;
            _myTarget = GameObject.FindGameObjectWithTag("Boss");

            other.GetComponent<ComboScoreComponent>().AddCombo(1);
                        
        }

    }
}
