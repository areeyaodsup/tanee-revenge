﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyColliderCheck : MonoBehaviour {

    void OnTriggerEnter(Collider player)
    {
       if (player.gameObject.tag.Contains("Player"))
        {
            Rigidbody rb = player.GetComponent<Rigidbody>();

            rb.AddForce(new Vector3(0,5,0),ForceMode.Impulse);
        }
    }
}
