﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrototypeTracking : MonoBehaviour {
    
    SphereCheck _checkActive;

    //public Vector3 _objectMovement = new Vector3(0.1f, 0, 0);
    public float speed;
   
    public void UpdateTracking(GameObject target,GameObject owner)
    {
        _checkActive = GetComponent<SphereCheck>();

        float step = speed * Time.deltaTime;
        if(target!=null)
        owner.transform.position = Vector3.MoveTowards(
            owner.transform.position, target.transform.position, step);
        if (_checkActive.activeArea == true)
            owner.transform.position += Vector3.zero;
        
        /*
        //Debug.Log("Update Tracking");
        _checkActive = GetComponent<SphereCheck>();

        Vector3 TargetPosiotion = target.transform.position;
        Vector3 vecToTargetNorm = TargetPosiotion - owner.transform.position;
        vecToTargetNorm.Normalize();

        _objectMovement = vecToTargetNorm / 10.0f;

        if (_checkActive.activeArea == true)
        {
            owner.transform.position += Vector3.zero;
        }
        else
        {
            owner.transform.position += _objectMovement*Time.deltaTime;

        }
        */
    }
    
}
