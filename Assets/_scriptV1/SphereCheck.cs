﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereCheck : MonoBehaviour {
    public Transform _object;
    //public Transform _enemies;
    public float radius = 3f;
    public bool activeArea = false;

   public void UpdateSphere(GameObject target) {
         Transform _enemies = target.GetComponent<Transform>();
         float distance = Vector3.Distance(_object.position,_enemies.position) ;
        if (distance <= radius)
        {
            activeArea = true;
           //Debug.Log("Object Active");
        }
        else
        {
            activeArea = false;
        }
	}
   
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(_object.position, radius);
    }
}
