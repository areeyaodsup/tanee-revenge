﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodHandBehavior : MonoBehaviour {
    GameObject _target;
    public int _force = 1;
	// Use this for initialization
	void Start () {
        _target = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
       Rigidbody rb = this.GetComponent<Rigidbody>();
       rb.velocity = Vector3.right*_force;
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PATK")
        {
            GameObject[] go = GameObject.FindGameObjectsWithTag("HandBoss");

            foreach(GameObject friend in go){

                Destroy(friend);
            }
            ComboScoreComponent sc = other.GetComponent<ComboScoreComponent>();
            sc.AddScore(150);
            sc.AddCombo(10);
        }
    }
}
