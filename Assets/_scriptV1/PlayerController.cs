﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour {
    Rigidbody _rigidbody;
    Vector3 _playerMovementSpeed = Vector3.zero;

    const float SCALE = 0.1f;
	// Use this for initialization
	void Start () {
        _rigidbody = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        _playerMovementSpeed.x = Input.GetAxis("Horizontal");
        _playerMovementSpeed.z = Input.GetAxis("Vertical");
        this.transform.position += _playerMovementSpeed * SCALE;
	}
}
