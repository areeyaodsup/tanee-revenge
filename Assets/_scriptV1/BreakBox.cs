﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collision))]
public class BreakBox : MonoBehaviour
{

    List<GameObject> _collidedList = new List<GameObject>();

    void Update()
    {
        Collider c = this.GetComponent<Collider>();
        
        if (c.enabled)
        {
            Invoke("DeactivateCollider", 1.0f);
            Invoke("TakeAction", 0.1f);
        }
    }

    void DeactivateCollider()
    {
        Collider c = this.GetComponent<Collider>();
        c.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Contains("Enemies")) 
            _collidedList.Add(other.gameObject);

    }

    private void TakeAction()
    {
        Vector3 thisGameObjectPos = this.gameObject.transform.position;
        GameObject shortestGameObject = null;
        float shortestValue = 999.0f;

        foreach (GameObject g in _collidedList)
        {
            Transform transG = g.GetComponent<Transform>();
            if(transG != null)
            {
                Vector3 v = g.GetComponent<Transform>().position;
                float currentDistance = Vector3.Distance(thisGameObjectPos, v);

                if (currentDistance < shortestValue)
                {
                    shortestValue = currentDistance;
                    shortestGameObject = g;
                }
            }
            else
            {
                Debug.Log("Transform is NULL.");
            }
           
        }

        if (shortestGameObject != null)
        {
            Collider c = this.GetComponent<Collider>();
            Renderer r = shortestGameObject.GetComponent<Renderer>();
            ComboScoreComponent csc = this.GetComponent<ComboScoreComponent>();
            Rigidbody rb = shortestGameObject.GetComponent<Rigidbody>();
            Vector3 boxPos = this.transform.position;
            boxPos.Normalize();
            csc.AddCombo(1);
            csc.AddScore(50);

            rb.AddForce(new Vector3(0, 1, 0)*10, ForceMode.Impulse);
            r.material.color = Color.yellow;
            _collidedList.Remove(shortestGameObject);
            Debug.Log("Destory enemies");
            Destroy(shortestGameObject,0.3f);
          

            c.enabled = false;
            
        }
     
    }
}
