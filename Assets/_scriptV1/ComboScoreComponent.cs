﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboScoreComponent : MonoBehaviour {

    public const float START_COMBO = 1.0f;

    protected int _score;
    public int Score
    {
        get { return _score; }
        set { _score = value; }
    }
    protected float _combo;
    public float Combo
    {
        get { return _combo; }
        set { _combo = value; }
    }

    void Start()
    {
        _combo = START_COMBO;
    }

    public void AddCombo(int amount)
    {
        _combo += amount;
       // Debug.Log("total combo "+_combo);
    }

    public void DivisCombo(float amount) //enemy atk
    {
        _combo = _combo / amount;
        if (_combo <= 0)
        {
            _combo = START_COMBO;
        }

       // Debug.Log("total combo " + _combo);
    }

    public void DecreaseCombo(float amount) //move state
    {
        _combo -= amount;
        if (_combo <= 0)
        {
            _combo = START_COMBO;
        }
        Debug.Log("decrease" + amount);
    }

    public void AddScore(int score)
    {
        int totalScore = 0;

        totalScore = score * (int)_combo;

        _score += totalScore;
    }

   

}
