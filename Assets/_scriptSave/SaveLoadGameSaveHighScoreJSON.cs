﻿using System.IO;
using UnityEngine;

public class SaveLoadGameSaveHighScoreJSON : ISaveLoadGameSaveHighScore
{
    public GameSaveHighScore LoadGameSaveHighScore(string location)
    {
        string json = "";
        StreamReader sr = new StreamReader(location);
        json = sr.ReadToEnd();
        sr.Close();

        return JsonUtility.FromJson<GameSaveHighScore>(json);
    }

    public void SaveGameSaveHighScore(GameSaveHighScore gsh, string location)
    {
        string json = JsonUtility.ToJson(gsh);

        StreamWriter sw = new StreamWriter(location);
        sw.Write(json);
        sw.Close();
    }
}
