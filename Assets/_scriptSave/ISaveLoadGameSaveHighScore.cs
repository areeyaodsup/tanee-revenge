﻿
interface ISaveLoadGameSaveHighScore {
    void SaveGameSaveHighScore(GameSaveHighScore gsh, string location);
    GameSaveHighScore LoadGameSaveHighScore(string location);
}
